package org.eghagha.csce;

import java.util.HashMap;

public class OperationFreq extends OperationSuperClass implements Strategy {

	private HashMap<String, Integer> hash = null;
	
	public OperationFreq(String address) {
		super(address);
	}

	@Override
	public void doOperation() {

		try {

			setHash(new HashMap<String, Integer>());
			/*
			 * use a hashmap to store words and their frequencies
			 */
			for (String s : wordsArray) {
				if (getHash().containsKey(s)) {
					getHash().replace(s, getHash().get(s) + 1);
				} else {
					getHash().put(s, 1);
				}
			}

			System.out.println(getHash());

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public String toString(){
		return  getHash().toString();
	}

	public HashMap<String, Integer> getHash() {
		return hash;
	}

	public void setHash(HashMap<String, Integer> hash) {
		this.hash = hash;
	}
	public boolean isThereWordsArray(){
		return (wordsArray.length > 0);
	}
}
