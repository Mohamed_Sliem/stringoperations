package org.eghagha.test;

import org.eghagha.csce.OperationWc;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.runners.Parameterized.*;

@RunWith(Parameterized.class)
public class OperationWcParameterizedTest {
	
	private OperationWc og;
	private String result = "";
	private String s1;
	private boolean b1;
	private String s = "fixbool";
	
	@Before 
	public void beforeTest(){
		og= new OperationWc(s1);
	}
	
	public OperationWcParameterizedTest(String s1, String result, boolean b1){
		super();
		this.s1 = s1;
		this.result = result;
		this.b1 = b1;
		
	}
		

    // creates the test data
    @Parameters
    public static Collection<Object[]> data() {
        Object[][] data = new Object[][] { { "10by10.txt", "209_100_10", true }, { "1by1.txt", "1_1_1", true }, { "0by0.txt", "0_0_0", true } };
        return Arrays.asList(data);
    }


	@Test
	public void testWordCount() {
		System.out.println("Word Count on a " + s1 + "file");
		og = new OperationWc(s1);
	    og.doOperation();		
        assertEquals("Result", result, og.toString());
        assertEquals("Result", b1, og.isThereAllWords());
        assertEquals("Result", b1, og.isThereLineArray());
      
    }


}